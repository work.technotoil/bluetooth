package com.blutooth.configuration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blutooth.R;
import com.blutooth.communication.MainActivity;

import java.util.ArrayList;

public class DeviceConfigActivity extends AppCompatActivity {
    private static final String TAG = "Device_Config";
    Button btn_scan;
    Context context = this;
    ProgressBar pb_process;
    TextView tv_scanning;
    ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();
    BluetoothAdapter mBluetoothAdapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_config);

        Log.d(TAG, "created");
        btn_scan = findViewById(R.id.btn_scan);
        pb_process = findViewById(R.id.pb_process);
        tv_scanning = findViewById(R.id.tv_scanning);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Scan new Device");

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, 0);
        }

        if (mBluetoothAdapter != null) {
           /* btn_scan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                }
            });*/

            mBluetoothAdapter.startDiscovery();

        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
    }

    @Override
    public void onPause() {
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
        }
        finish();
        super.onPause();
    }

    @Override
    protected void onStop() {
        mBluetoothAdapter.cancelDiscovery();
        finish();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        finish();
        super.onDestroy();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter
                        .EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    showToast("Enabled");
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED
                    .equals(action)) {
                mDeviceList = new ArrayList<BluetoothDevice>();
                pb_process.setVisibility(View.VISIBLE);
                tv_scanning.setVisibility(View.VISIBLE);
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                pb_process.setVisibility(View.GONE);
                tv_scanning.setVisibility(View.GONE);

                if (mDeviceList.size() > 0) {
                    Intent newIntent = new Intent(context, UnPairedListActivity.class);
                    newIntent.putParcelableArrayListExtra("device.list", mDeviceList);
                    startActivity(newIntent);
                } else {
                    tv_scanning.setVisibility(View.VISIBLE);
                    tv_scanning.setText("Device not found");
                }

                finish();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                int state = intent.getIntExtra(BluetoothDevice
                        .EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                if (state != BluetoothDevice.BOND_BONDED) {
                    mDeviceList.add(device);
                    showToast("Found device " + device.getName());
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}
