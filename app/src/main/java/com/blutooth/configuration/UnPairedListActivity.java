package com.blutooth.configuration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blutooth.R;
import com.blutooth.communication.BlutoothConnectActivity;
import com.blutooth.communication.MainActivity;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class UnPairedListActivity extends AppCompatActivity {
    Toolbar toolbar;

    ListView mListView;
    UnpairDeviceAdapter mAdapter;
    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<>();
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_paired_list);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Pair Device");

        mDeviceList = getIntent().getExtras().getParcelableArrayList("device.list");
        mListView = findViewById(R.id.list);
        mAdapter = new UnpairDeviceAdapter(this);
        mAdapter.setData(mDeviceList);
        mListView.setAdapter(mAdapter);
        registerReceiver(mPairReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
        registerReceiver(mPairRequest, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST));
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mPairReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(UnPairedListActivity.this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);
                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    showToast("Paired");
                    Intent intent_ = new Intent(UnPairedListActivity.this, MainActivity.class);
                    startActivity(intent_);
                    finish();
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
                    showToast("Unpaired");
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    };
    private final BroadcastReceiver mPairRequest = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothDevice.ACTION_PAIRING_REQUEST)) {

            }
        }
    };

    public class UnpairDeviceAdapter extends BaseAdapter {
        Context context;
        private LayoutInflater mInflater;
        private List<BluetoothDevice> mData;

        public UnpairDeviceAdapter(Context context) {
            this.context = context;
            mInflater = LayoutInflater.from(context);
        }

        public void setData(List<BluetoothDevice> data) {
            mData = data;
        }

        public int getCount() {
            return (mData == null) ? 0 : mData.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.devices_list, parent, false);
                holder = new ViewHolder();
                holder.unpair_name = (TextView) convertView.findViewById(R.id.unpair_device_name);
                holder.unpair_add = (TextView) convertView.findViewById(R.id.unpair_device_address);
                holder.unpair = (Button) convertView.findViewById(R.id.unpair_btn);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            BluetoothDevice device = mData.get(position);
            holder.unpair_name.setText(device.getName());
            holder.unpair_add.setText(device.getAddress());
            holder.unpair.setText((device.getBondState() == BluetoothDevice.BOND_BONDED) ? "Unpair" : "Pair");
            holder.unpair.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BluetoothDevice device = mDeviceList.get(position);
                    showToast("Pairing...");
                    pairDevice(device);
                    mDeviceList.remove(position);
                    notifyDataSetChanged();
                }
            });
            return convertView;
        }

        class ViewHolder {
            TextView unpair_name;
            TextView unpair_add;
            Button unpair;
        }
    }
}
