package com.blutooth.communication;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blutooth.R;

import java.util.ArrayList;

public class BluetoothSelectAdapter extends BaseAdapter {

    private ArrayList<BluetoothDevice> _list = new ArrayList<>();
    private Context context;

    public BluetoothSelectAdapter(BlutoothConnectActivity blutoothConnectActivity, ArrayList<BluetoothDevice> list) {
        this.context = blutoothConnectActivity;
        this._list = list;
    }

    @Override
    public int getCount() {
        return _list.size();
    }

    @Override
    public Object getItem(int position) {
        return _list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            view = new View(context);
            view = inflater.inflate(R.layout.home_list_item,
                    parent, false);
            ImageView imageView = view
                    .findViewById(R.id.list_image);
            TextView textview = view
                    .findViewById(R.id.list_caption);
            imageView.setImageResource(R.drawable.ic_phone);
            textview.setText(_list.get(position).getName());
        } else {
            view = (View) convertView;
        }
        return view;
    }
}

