package com.blutooth.controls;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.blutooth.R;
import com.blutooth.communication.BluetoothChatService;
import com.blutooth.configuration.PairListBean;

import java.util.ArrayList;

public class EditMessageActivity extends AppCompatActivity {
    private static final String TAG = "EditMessageActivity";
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static String STATUS = "status";
    private static final int REQUEST_CONNECT_DEVICE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    static String mConnectedDeviceName = null, MAC = "",
            receiveMessage = "", name = "", type = "";
    boolean checkStatus = false;

    ArrayList<byte[]> messageByte = new ArrayList<byte[]>();
    ToggleButton tg;
    ImageView ivbulb;
    BluetoothAdapter mBluetoothAdapter = null;
    BluetoothChatService mDataService = null;
    Intent intent;
    Bundle bundle;
    PairListBean _bean;
    ProgressDialog _progress;
    ProgressBar pb_process;

    TextView tv_connectDevice, tv_showMessage;
    AutoCompleteTextView et_EditData;
    Toolbar toolbar;
    Context context = this;
    private StringBuffer mOutStringBuffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_message);

        if (getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type");
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Enter Message");

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        intent = getIntent();
        bundle = intent.getExtras();
        if (getIntent().getSerializableExtra("device.data") != null) {
            _bean = (PairListBean) bundle.getSerializable("device.data");
            MAC = _bean.getMac();
            name = _bean.getName();
        }
        getVariable();
        setupChat();
        if (!MAC.equals("")) {
            connectDevice();
        }

        if (type.equals("show")) {
            et_EditData.setVisibility(View.GONE);
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mDataService != null) mDataService.stop();
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");
        mDataService = new BluetoothChatService(context, mHandler);
        mOutStringBuffer = new StringBuffer("");
    }

    private void sendMessages(String message) {
        byte[] send = message.getBytes();
        mDataService.write(send);
    }

    private void receiveMessage(String readMessage) {
        String device = readMessage.substring(1, 2);
        String status = readMessage.substring(2, 3);
        tv_showMessage.setText(mConnectedDeviceName + ":  "
                + readMessage);
        if (status.equals("0")) {
            checkStatus = false;
            if (device.equals("1")) {
                tg.setChecked(false);
            }
        }
        if (status.equals("1")) {
            checkStatus = true;
            if (device.equals("1")) {
                tg.setChecked(true);
            }

        }
        receiveMessage = "";
    }

    @Override
    public void onBackPressed() {
        messageByte.clear();
        // stop service and thread
        mDataService.stop();
        super.onBackPressed();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String writeMessage = "", readMsg;
            switch (msg.what) {
                case MESSAGE_WRITE:
                    // write new message
                    byte[] writeBuf = (byte[]) msg.obj;
                    messageByte.add(writeBuf);
                    writeMessage = new String(messageByte.get(0));
                    if (writeMessage.length() > 1) {
                        sendMessages(writeMessage);
                    }
                    tv_showMessage.setText("");
                    break;
                case MESSAGE_READ:
                    // read received message from here
                    byte[] readBuf = (byte[]) msg.obj;
                    readMsg = new String(readBuf, 0, msg.arg1);
                    String what = msg.what + "";
                    if (readMsg.length() > 1) {
                        receiveMessage = what + readMsg;
                        receiveMessage(receiveMessage);
                    }
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    pb_process.setVisibility(View.GONE);
                    tv_connectDevice.setText(mConnectedDeviceName);
                    break;
                case MESSAGE_TOAST:
                    STATUS = msg.getData().getString(TOAST);
                    if (STATUS.equals("Unable to connect device")) {
                        Toast.makeText(context, "Error while establishing connection", Toast.LENGTH_SHORT).show();
                    }
                    pb_process.setVisibility(View.GONE);
                    EditMessageActivity.this.finish();
                    break;
            }
        }
    };

    private void connectDevice() {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(MAC);
        mDataService.connect(device);
    }

    public void getVariable() {
        tv_showMessage = findViewById(R.id.tv_showMessage);
        pb_process = findViewById(R.id.pb_process);
        et_EditData = findViewById(R.id.et_enterMessage);
        ivbulb = findViewById(R.id.ivbulb);
        tg = findViewById(R.id.tg);
        tg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //  _progress.setMessage("Sending Signal");
                pb_process.setVisibility(View.VISIBLE);
                if (isChecked) {
                    ivbulb.setImageResource(R.drawable.on_bulb);
                }
                if (!isChecked) {
                    ivbulb.setImageResource(R.drawable.off_bulb);
                }
            }
        });
        et_EditData.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                sendMessage(et_EditData.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sendMessage(et_EditData.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                sendMessage(et_EditData.getText().toString());
            }
        });

        tv_connectDevice = findViewById(R.id.tv_connectDevice);
    }

    private void sendMessage(String message) {

        // Check that we're actually connected before trying anything
        if (mDataService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mDataService.write(send);
            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            et_EditData.setText(mOutStringBuffer);
        }
    }
}
