package com.blutooth.configuration;

import java.io.Serializable;
/**
 * Bean class for paired device
 * @author Mohit on 03-Nov-2015 at 3:32:40 pm
 *
 */
public class PairListBean implements Serializable{

	private static final long serialVersionUID = 359674333662835107L;
	private String name;
	private String mac;

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	
}