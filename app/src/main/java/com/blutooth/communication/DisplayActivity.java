package com.blutooth.communication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.blutooth.R;
import com.blutooth.controls.EditMessageActivity;

import java.util.ArrayList;

public class DisplayActivity extends AppCompatActivity {

    private static final String TAG = "DisplayActivity";

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    TextView tv_connectDevice, tv_displayData;
    Toolbar toolbar;
    ToggleButton tg;
    Context context = this;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService mDataService = null;
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static String STATUS = "status";
    static String mConnectedDeviceName = null, MAC = "",
            receiveMessage = "", name = "", type = "";
    boolean checkStatus = false;
    ArrayList<byte[]> messageByte = new ArrayList<byte[]>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diplay);

        toolbar = findViewById(R.id.toolbar);
        tg = findViewById(R.id.tg);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Display Data");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_connectDevice = findViewById(R.id.tv_connectDevice);
        tv_displayData = findViewById(R.id.tv_displayData);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mDataService == null) setupChat();
        }
    }
    @Override
    public synchronized void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mDataService != null) mDataService.stop();
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");
        mDataService = new BluetoothChatService(context, mHandler);
    }

    private void sendMessages(String message) {
        byte[] send = message.getBytes();
        mDataService.write(send);
    }
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mDataService != null) {
            if (mDataService.getState() == BluetoothChatService.STATE_NONE) {
                mDataService.start();
            }
        }
    }

    private void receiveMessage(String readMessage) {
        String device = readMessage.substring(1, 2);
        String status = readMessage.substring(2, 3);
        tv_displayData.setText(mConnectedDeviceName + ":  "
                + readMessage);
        if (status.equals("0")) {
            checkStatus = false;
            if (device.equals("1")) {
                tg.setChecked(false);
            }
        }
        if (status.equals("1")) {
            checkStatus = true;
            if (device.equals("1")) {
                tg.setChecked(true);
            }

        }
        receiveMessage = "";
    }

    @Override
    public void onBackPressed() {
        messageByte.clear();
        // stop service and thread
        mDataService.stop();
        super.onBackPressed();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String writeMessage = "", readMsg;
            switch (msg.what) {
                case MESSAGE_WRITE:
                    // write new message
                    byte[] writeBuf = (byte[]) msg.obj;
                    messageByte.add(writeBuf);
                    writeMessage = new String(messageByte.get(0));
                    if (writeMessage.length() > 1) {
                        sendMessages(writeMessage);
                    }
                    tv_displayData.setText("");
                    break;
                case MESSAGE_READ:
                    // read received message from here
                    byte[] readBuf = (byte[]) msg.obj;
                    readMsg = new String(readBuf, 0, msg.arg1);
                    String what = msg.what + "";
                    if (readMsg.length() > 1) {
                        receiveMessage = what + readMsg;
                        receiveMessage(receiveMessage);
                    }
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    //pb_process.setVisibility(View.GONE);
                    tv_connectDevice.setText(mConnectedDeviceName);
                    break;
                case MESSAGE_TOAST:
                    STATUS = msg.getData().getString(TOAST);
                    if (STATUS.equals("Unable to connect device")) {
                        Toast.makeText(getApplicationContext(),
                                "Error while establishing connection", Toast.LENGTH_SHORT).show();
                    }
                    //    pb_process.setVisibility(View.GONE);
                    DisplayActivity.this.finish();
                    break;
            }
        }
    };

    private void connectDevice() {
        BluetoothDevice device = mBluetoothAdapter
                .getRemoteDevice(MAC);
        mDataService.connect(device);
    }
}
